# Scenario 3

You are trying to add existing host to your automation scheme that manages your infrastructure. You try to run one of your existing playbooks against but it fails.

Bring that host back to compliance and make the playbook work.

# Solution

```bash
ansible-playbook examples/3/break_fix/scenario_3/common_compliance_playbook.yml --ask-pass --become-method=su --ask-become-pass
```

The playbook has to be initially run with these parameters

`--ask-pass` because we do not have public-key authentication configured
`--become-method=su` because sudo is not working
`--ask-become-pass` because we use `su` auth method and that always require password

# Verification

Run the playbook again without the above additional parameters

