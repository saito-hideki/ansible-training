# Scenario 10

Keeping organization of your variables sane can be daunting task.

Take a look on this repository of examples how are variables exposed via different mechanism of Ansible Engine as well as how they are going to change in the next release

https://github.com/ansiblejunky/ansible-example-role-vars
