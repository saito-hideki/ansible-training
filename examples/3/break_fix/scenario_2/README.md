# Scenario 2

You need to create a set of temporary files that you then iterate over in the follow-up task. You have tested your solution on `localhost` and everything has worked as expected. However when you tried it against production server you see that the iteration task is empty.

What is the cause of the issue?

How to get it back to the working state?
