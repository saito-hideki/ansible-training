# Scenario 9

This scenario has several parts since Tower has several services and their problems can present themselves via different malfunctions.

# Example 1

Go to your Tower installation and disable `memcached` service. Re-run some job template and observe that you cannot look at details of the stdout as well as metrics about that run are not being updated.

## Explanation

`Memcached` service is used for storing data from the callback plugin that is sending data about playbook execution. When this service is not working the stdout of the run is not working properly

# Example 2

Go to your Tower installation and remove directory `/var/lib/rabbitmq/mnesia`. Observe that you won't be able to run any job.

## Explanation

`RabbitMQ` service is the broker and backbone of workers communication. If it's not working the jobs can be scheduled to nodes and nothing is working. This was a cruel way of showing it but it is equivalent to scenarios when the `mnesia database` gets broken and Tower node is not capable of functioning.

The only way how to recover is to get rid of such broken settings (obviously we have done that) and re-run the installer to initialize RabbitMQ again.

